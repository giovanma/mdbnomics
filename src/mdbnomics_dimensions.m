function dimensions = mdbnomics_dimensions(varargin) % --*-- Unitary tests --*--
% function mdbnomics_dimensions(varargin)
% Downloads the list of dimensions (if they exist) for available datasets of a selection of providers from https://db.nomics.world/.
% By default, the function returns a structure containing the dimensions of datasets for DBnomics providers.
%
% POSSIBLE PARAMETERS
%   provider_code                [char]        DBnomics code of one or multiple providers. If empty, the providers are firstly 
%                                              dowloaded with the function mdbnomics_providers and then the available datasets are requested.
%   dataset_code                 [char]        DBnomics code of one or multiple datasets of a provider. If empty, the datasets codes are dowloaded 
%                                              with the function mdbnomics_datasets and then the dimensions are requested.
%   simplify                     [logical]     If true, when the dimensions are requested for only one provider and one dataset then only the dimension names and their values are provided.
%                                              If not provided, the default value is false.
%
% OUTPUTS
%   dimensions
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global mdb_options

p = inputParser;
validStringInput = @(x) ischar(x) || iscellstr(x);
p.addParameter('provider_code', '', validStringInput);
p.addParameter('dataset_code', '', validStringInput);
p.addParameter('simplify', false, @islogical);
p.KeepUnmatched = false;
p.parse(varargin{:});

if isempty(p.Results.provider_code) && ~isempty(p.Results.dataset_code)
    error('When you use dataset_code, you must specify provider_code as well.');
end

if iscell(p.Results.provider_code) || iscell(p.Results.dataset_code)
    if ~isempty(p.Results.provider_code) && ~isempty(p.Results.dataset_code) && length(p.Results.provider_code) ~= length(p.Results.dataset_code)
        error('Please specify as many provider codes as dataset codes.')
    end
end

if isempty(p.Results.provider_code)
    provider_code = mdbnomics_providers('code', true);
else
    if ischar(p.Results.provider_code)
        provider_code = {p.Results.provider_code};
    else
        provider_code = p.Results.provider_code;
    end
end

if isempty(p.Results.dataset_code)
    dataset_code = mdbnomics_datasets('provider_code', provider_code);
else
    if ischar(p.Results.dataset_code)
        dataset_code = {p.Results.dataset_code};
    else
        dataset_code = p.Results.dataset_code;
    end
end

dimensions = struct();
for i = 1:numel(provider_code)
    pc = provider_code{i};
    dc = dataset_code{i};
    dataset_page = sprintf('%s/v%d/datasets/%s/%s', mdb_options.api_base_url, mdb_options.api_version, pc, dc);
    dataset_info = webread(dataset_page);
    dataset_name = sprintf('%s_%s', pc, dc);
    
    try
        tmp1 = dataset_info.datasets.docs.dimensions_labels;
    catch
        try
            tmp1 = dataset_info.datasets.(dataset_name).dimensions_labels;
        catch
            tmp1 = {};
        end
    end

    try
        tmp2 = dataset_info.datasets.docs.dimensions_values_labels;
    catch
        try
            tmp2 = dataset_info.datasets.(dataset_name).dimensions_values_labels;
        catch
            tmp2 = {};
        end
    end
    
    dataset_dimensions = fieldnames(tmp1);
    for d = 1:numel(dataset_dimensions)
        dimensions.(dataset_name).(dataset_dimensions{d}) = tmp2.(dataset_dimensions{d});
    end
end

if p.Results.simplify
    if length(fieldnames(dimensions)) == 1
        dimensions = dimensions.(dataset_name);
    else
        error('Your query corresponds to multiple datasets, not possible to simplify');
    end
end
end

%@test:1
%$ try
%$    dimensions = mdbnomics_dimensions('provider_code', 'IMF', 'dataset_code', 'WEO');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(fieldnames(dimensions), {'IMF_WEO'}); 
%$    t(3) = dassert(isfield(dimensions.IMF_WEO, 'unit'), true);
%$    t(4) = dassert(length(fieldnames(dimensions.IMF_WEO.unit)), 13);
%$ end 
%$
%$ T = all(t);
%@eof:1

%@test:2
%$ try
%$    dimensions = mdbnomics_dimensions('provider_code', 'IMF', 'dataset_code', 'WEO', 'simplify', true);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(isfield(dimensions, 'IMF_WEO'), false); 
%$    t(3) = dassert(isfield(dimensions, 'unit'), true);
%$    t(4) = dassert(length(fieldnames(dimensions.unit)), 13);
%$ end 
%$
%$ T = all(t);
%@eof:2
