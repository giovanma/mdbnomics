function df = mdbnomics(varargin) % --*-- Unitary tests --*--
% function mdbnomics(varargin)
% Download time series from DBnomics. 
% Returns a cell array.
%
% Examples:
% Fetch one series:
% mdbnomics('provider_code', 'IMF', 'dataset_code', 'CPI', 'series_code', 'M.FR+DE.PCPIEC_IX+PCPIA_IX');
% mdbnomics('provider_code', 'IMF', 'dataset_code', 'CPI', 'series_code', '.FR.PCPIEC_WT');
% 
% Fetch all the series of a dataset:
% mdbnomics('provider_code', 'AMECO', 'dataset_code', 'UVGD', 'max_nb_series', 500);
% 
% Fetch many series from different datasets:
% mdbnomics('series_ids', {'AMECO/ZUTN/EA19.1.0.0.0.ZUTN', 'AMECO/ZUTN/DNK.1.0.0.0.ZUTN', 'IMF/CPI/A.AT.PCPIT_IX'});
% 
% Fetch many series from the same dataset, searching by dimension:
% mdbnomics('provider_code','AMECO', 'dataset_code', 'ZUTN', 'dimensions', '{"geo":["dnk"]}');
%
% Fetch series given an "API link" URL.
% "API link" URLs can be found on DBnomics web site (https://db.nomics.world/) on dataset or series pages using "Download" buttons.
% mdbnomics('api_link', 'https://api.db.nomics.world/v22/series?series_ids=AMECO%2FZUTN%2FEA19.1.0.0.0.ZUTN&observations=1');
% 
% POSSIBLE PARAMETERS
%   provider_code           [string]     the code of the dataset provider.  
%   dataset_code            [string]     the code of the dataset.  
%   series_code             [string]     the code mask of the series. If provided, the provider_code and the dataset_code must specified.         
%   dimensions              [char]       dataset dimension codes. If provided it must be a string formatted like: '{"country":["ES","FR","IT"],"indicator":["IC.REG.COST.PC.FE.ZS.DRFN"]}'.
%   series_ids              [string]     list of series IDs. It is string formatted like `provider_code/dataset_code/series_code`. If provided, the provider_code and the dataset_code shouldn't be specified.
%   max_nb_series           [integer]    maximum number of series requested by the API. If not provided, a default value of 50 series will be used.
%   api_base_url            [string]     the base URL used for API requests. If not provided, a default value of: 'https://api.db.nomics.world/v22/' will be used.       
%   dbnomics_filters        [char]       filters to apply on the requested series. If provided it must be a string formatted like: '[{"code": "interpolate", "parameters": {"frequency": "monthly", "method": "spline"}}]'.
%   api_link                [char]       fetch series given an "API link" URL.
%
% OUTPUTS
%   df
%
% SPECIAL REQUIREMENTS
%   When you (don't) use dimensions, you must specifiy provider_code and dataset_code.
%   When you use series_code, you must specifiy provider_code and dataset_code.
%   When you use series_ids, you must not specifiy provider_code nor dataset_code.

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global mdb_options

default_api_base_url = sprintf('%s/v%d/', mdb_options.api_base_url, mdb_options.api_version);
default_editor_base_url = sprintf('%s/api/v%d/', mdb_options.editor_base_url, mdb_options.editor_version);

p = inputParser;
validStringInput = @(x) ischar(x) || iscellstr(x);
p.addParameter('provider_code', '', validStringInput);
p.addParameter('dataset_code', '', validStringInput);
p.addParameter('series_code', '', validStringInput); 
p.addParameter('dimensions', @ischar);
p.addParameter('series_ids', '',validStringInput);
p.addParameter('max_nb_series', NaN, @isnumeric);
p.addParameter('api_base_url', default_api_base_url, validStringInput);
p.addParameter('dbnomics_filters', '', @ischar);
p.addParameter('api_link', '', @ischar);
p.KeepUnmatched = false;
p.parse(varargin{:});

if strcmp(p.Results.api_base_url(end),'/') == 0
    p.Results.api_base_url = [p.Results.api_base_url '/'];
end

if isempty(p.Results.dataset_code)
    if iscell(p.Results.provider_code)
        p.Results.series_ids = p.Results.provider_code;
        p.Results.provider_code = '';
    elseif ~isempty(p.Results.provider_code)
        p.Results.series_ids = {p.Results.provider_code};
        p.Results.provider_code = '';
    end
end

series_base_url = [p.Results.api_base_url 'series'];

if isa(p.Results.dimensions, 'function_handle') && isempty(p.Results.series_code) && isempty(p.Results.series_ids)
    if (isempty(p.Results.provider_code) || isempty(p.Results.dataset_code)) && isempty(p.Results.api_link)
        error('When you don''t use dimensions, you must specifiy provider_code and dataset_code.');
    end
    api_link = sprintf('%s/%s/%s?observations=1', series_base_url, p.Results.provider_code, p.Results.dataset_code);
end

if ~isa(p.Results.dimensions, 'function_handle')
    if isempty(p.Results.provider_code) || isempty(p.Results.dataset_code)
        error('When you use dimensions, you must specifiy provider_code and dataset_code.');
    end
    api_link = sprintf('%s/%s/%s?observations=1&dimensions=%s', series_base_url, p.Results.provider_code, p.Results.dataset_code, p.Results.dimensions);
end

if ~isempty(p.Results.series_code)
    if isempty(p.Results.provider_code) || isempty(p.Results.dataset_code)
        error('When you use series_code, you must specifiy provider_code and dataset_code.');
    end
    api_link = sprintf('%s/%s/%s/%s?observations=1', series_base_url, p.Results.provider_code, p.Results.dataset_code, p.Results.series_code);
end

if ~isempty(p.Results.series_ids)
    if ~isempty(p.Results.provider_code) || ~isempty(p.Results.dataset_code)
        error('When you use series_ids, you must not specifiy provider_code nor dataset_code.');
    end
    if iscellstr(p.Results.series_ids)
        series_ids = strjoin(p.Results.series_ids,',');
    else
        series_ids = p.Results.series_ids;
    end
    api_link = sprintf('%s?observations=1&series_ids=%s', series_base_url, series_ids);
end

if ~isempty(p.Results.api_link)
    api_link = p.Results.api_link;
end

df = fetch_series_by_api_link(api_link, p.Results.dbnomics_filters, p.Results.max_nb_series, default_editor_base_url);
end

%@test:1    % test_fetch_series_by_code
%$ try
%$    df = mdbnomics('provider_code', 'AMECO', 'dataset_code', 'ZUTN', 'series_code', 'EA19.1.0.0.0.ZUTN');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),1); 
%$    t(7) = dassert(df(2,5), {'EA19.1.0.0.0.ZUTN'}); 
%$ end 
%$
%$ T = all(t);
%@eof:1

%@test:2    % test_fetch_series_by_code_mask
%$ try
%$    df = mdbnomics('provider_code', 'IMF', 'dataset_code', 'CPI', 'series_code', 'M.FR+DE.PCPIEC_IX+PCPIA_IX');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'IMF'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'CPI'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),4); 
%$ end 
%$
%$ T = all(t);
%@eof:2

%@test:3    % test_fetch_series_by_code_mask_with_plus
%$ try
%$    df = mdbnomics('provider_code', 'SCB', 'dataset_code', 'AKIAM', 'series_code', '"J+K"+"G+H".AM0301C1');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'SCB'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'AKIAM'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),2); 
%$ end 
%$
%$ T = all(t);
%@eof:3

%@test:4    % test_fetch_series_by_dimension
%$ try
%$    df = mdbnomics('provider_code','WB','dataset_code','DB', 'dimensions', '{"country":["ES","FR","IT"],"indicator":["IC.REG.COST.PC.FE.ZS.DRFN"]}');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'WB'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'DB'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),3); 
%$ end 
%$
%$ T = all(t);
%@eof:4

%@test:5    % test_fetch_series_by_id
%$ try
%$    df = mdbnomics('series_ids','AMECO/ZUTN/EA19.1.0.0.0.ZUTN');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),1); 
%$    t(7) = dassert(df(2,5), {'EA19.1.0.0.0.ZUTN'});
%$ end 
%$
%$ T = all(t);
%@eof:5

%@test:6    % test_fetch_series_by_ids_in_different_datasets
%$ try
%$    df = mdbnomics('series_ids', {'AMECO/ZUTN/EA19.1.0.0.0.ZUTN', 'BIS/cbs/Q.S.5A.4B.F.B.A.A.LC1.A.1C'});
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1)
%$    provider_codes = unique(df(2:end,2));
%$    t(2) = dassert(length(provider_codes),2); 
%$    t(3) = dassert(provider_codes{1}, 'AMECO');
%$    t(4) = dassert(provider_codes{2}, 'BIS');
%$    dataset_codes = unique(df(2:end,3));
%$    t(5) = dassert(length(dataset_codes),2); 
%$    t(6) = dassert(dataset_codes{1}, 'ZUTN');
%$    t(7) = dassert(dataset_codes{2}, 'cbs');
%$    series_codes = unique(df(2:end,5));
%$    t(8) = dassert(length(series_codes),2); 
%$    t(9) = dassert(series_codes{1}, 'EA19.1.0.0.0.ZUTN');
%$    t(10) = dassert(series_codes{2}, 'Q.S.5A.4B.F.B.A.A.LC1.A.1C');
%$ end 
%$
%$ T = all(t);
%@eof:6

%@test:7    % test_fetch_series_by_ids_in_same_dataset
%$ try
%$    df = mdbnomics('series_ids', {'AMECO/ZUTN/EA19.1.0.0.0.ZUTN',...
%$                                     'AMECO/ZUTN/DNK.1.0.0.0.ZUTN'});
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'}); 
%$    series_codes = unique(df(2:end,5));
%$    t(6) = dassert(length(series_codes),2); 
%$    t(7) = dassert(series_codes{1}, 'DNK.1.0.0.0.ZUTN');
%$    t(8) = dassert(series_codes{2}, 'EA19.1.0.0.0.ZUTN');
%$ end 
%$
%$ T = all(t);
%@eof:7

%@test:8    % test_fetch_series_of_dataset
%$ try
%$    df = mdbnomics('provider_code', 'AMECO', 'dataset_code', 'ZUTN');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))), 48); 
%$ end 
%$
%$ T = all(t);
%@eof:8

%@test:9    % test_fetch_series_with_filter_on_one_series
%$ try
%$    filters_ = '[{"code": "interpolate", "parameters": {"frequency": "monthly", "method": "spline"}}]';
%$    df = mdbnomics('provider_code', 'AMECO', 'dataset_code', 'ZUTN', 'series_code', 'DEU.1.0.0.0.ZUTN', 'dbnomics_filters', filters_);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'});
%$    series_codes = unique(df(2:end,5));
%$    t(6) = dassert(length(unique(df(2:end,5))),2);
%$    t(7) = dassert(df(2,5), {'DEU.1.0.0.0.ZUTN'});
%$    t(8) = dassert(series_codes{2}, 'DEU.1.0.0.0.ZUTN_filtered');
%$    freq_ = unique(df(2:end, 1));
%$    t(9) = dassert(freq_{2}, 'monthly');
%$    t(10) = dassert(df(1,12), {'filtered'});
%$ end 
%$
%$ T = all(t);
%@eof:9

%@test:10    % test_fetch_series_with_max_nb_series
%$ try
%$    df = mdbnomics('provider_code', 'AMECO', 'dataset_code', 'ZUTN', 'max_nb_series',20);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'});
%$    series_codes = unique(df(2:end,5));
%$    assert(length(series_codes) <= 20);
%$ end 
%$
%$ T = all(t);
%@eof:10

%@test:11    % test_fetch_series_with_na_values
%$ try
%$    df = mdbnomics('provider_code', 'AMECO', 'dataset_code', 'ZUTN', 'series_code', 'DEU.1.0.0.0.ZUTN');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'AMECO'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'ZUTN'});
%$    t(6) = dassert(length(unique(df(2:end,5))),1); 
%$    t(7) = dassert(df(2,5), {'DEU.1.0.0.0.ZUTN'});
%$    assert(any(strcmp('NA', df(2:end,9))) == true);
%$    assert(any(isnan(cell2mat(df(2:end,10)))) == true);
%$ end 
%$
%$ T = all(t);
%@eof:11

%@test:12    % test_fetch_series_by_api_link
%$ try
%$    df = mdbnomics('api_link', 'https://api.db.nomics.world/v22/series/BIS/long_pp?limit=1000&offset=0&q=&observations=1&align_periods=1&dimensions=%7B%7D');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'BIS'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'long_pp'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),23); 
%$ end 
%$
%$ T = all(t);
%@eof:12
