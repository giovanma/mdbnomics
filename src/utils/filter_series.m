function filtered_series  = filter_series(series_list, dbnomics_filters, editor_api_base_url)
% function filter_series(series_list, dbnomics_filters, editor_api_base_url)
% Adapts editor_api_base_url and returns the cell array of filtered series.
%
% INPUTS
%   series_list             [cell array]       cell array of series previously requested
%   dbnomics_filters        [string]           string array of filters to apply on series
%   editor_api_base_url     [string]           editor API link
%
% OUTPUTS
%   filtered_series
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

if strcmp(editor_api_base_url(end),'/') == 0
    editor_api_base_url = strcat(editor_api_base_url, '/');
end

apply_endpoint_url = strcat(editor_api_base_url,'apply');
filtered_series = iter_filtered_series(series_list, dbnomics_filters, apply_endpoint_url);
end

