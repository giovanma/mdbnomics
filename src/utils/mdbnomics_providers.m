function providers = mdbnomics_providers(varargin) % --*-- Unitary tests --*--
% function mdbnomics_providers(varargin)
% Downloads the list of DBnomics providers from https://db.nomics.world/.
% By default, the function returns a cell array containing the list of providers 
% with additional informations such as the region, the website, etc.
%
% POSSIBLE PARAMETERS
%   code                [logical]        If true, then only the providers are returned in a vector. If not provided, the default value is false.    
%
% OUTPUTS
%   providers
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global mdb_options

p = inputParser;
p.addParameter('code', false, @islogical);
p.KeepUnmatched = false;
p.parse(varargin{:});

providers_url = sprintf('%s/v%d/providers', mdb_options.api_base_url, mdb_options.api_version);
response = webread(providers_url);

if p.Results.code
    providers = cell(size(response.providers.docs, 1),1);
    for i = 1:size(response.providers.docs, 1)
        providers{i} = response.providers.docs{i}.code;
    end
    providers(cellfun('isempty',providers)) = [];
else
    providers = response.providers.docs;
end
end

%@test:1
%$ try
%$    providers = mdbnomics_providers('code', true);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(providers)), 67); 
%$    t(3) = dassert(providers{1}, 'AFDB'); 
%$    t(4) = dassert(providers{67}, 'WTO'); 
%$ end 
%$
%$ T = all(t);
%@eof:1

%@test:2
%$ try
%$    providers = mdbnomics_providers('code', false);
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(isstruct(providers{1}), true); 
%$    t(3) = dassert(size(providers,1), 67); 
%$    t(4) = dassert(providers{1}.code, 'AFDB'); 
%$    t(5) = dassert(providers{67}.code, 'WTO'); 
%$ end 
%$
%$ T = all(t);
%@eof:2

%@test:3
%$ try
%$    providers = mdbnomics_providers();
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(isstruct(providers{1}), true); 
%$    t(3) = dassert(size(providers,1), 67); 
%$    t(4) = dassert(providers{1}.code, 'AFDB'); 
%$    t(5) = dassert(providers{67}.code, 'WTO'); 
%$ end 
%$
%$ T = all(t);
%@eof:3
