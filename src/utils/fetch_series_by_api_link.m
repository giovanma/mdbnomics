function df = fetch_series_by_api_link(api_link, varargin) % --*-- Unitary tests --*--  
% function fetch_series_by_api_link(api_link, varargin)
% Fetch series given an "API link" URL.
% "API link" URLs can be found on DBnomics web site (https://db.nomics.world/) on dataset or series pages using "Download" buttons.
% Returns a cell array.
%
% Example:
% fetch_series_by_api_link('https://api.db.nomics.world/v22/series?series_ids=AMECO%2FZUTN%2FEA19.1.0.0.0.ZUTN&observations=1');
% 
% REQUIRED PARAMETERS
%   api_link                [string]     the URL used for the API request.
% 
% OPTIONAL PARAMETERS
%   dbnomics_filters        [char]       filters to apply on the requested series. If provided it must be a string formatted like: '[{"code": "interpolate", "parameters": {"frequency": "monthly", "method": "spline"}}]'.
%   max_nb_series           [integer]    maximum number of series requested by the API. If not provided, a default value of 50 series will be used.
%   editor_api_base_url     [string]     the editor URL used for API requests. If not provided, a default value of: 'https://editor.nomics.world/api/v1/' will be used.       
%
% OUTPUTS
%   df
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global mdb_options

default_editor_base_url = sprintf('%s/api/v%d/', mdb_options.editor_base_url, mdb_options.editor_version);

p = inputParser;
validStringInput = @(x) ischar(x) || iscellstr(x);
p.addRequired('api_link');
p.addOptional('dbnomics_filters', '',@ischar);
p.addOptional('max_nb_series', NaN, @isnumeric);
p.addOptional('editor_api_base_url', default_editor_base_url, validStringInput);
p.KeepUnmatched = false;
p.parse(api_link, varargin{:});

[datasets_dimensions, series_dims_by_dataset_code, series_list]= iter_series_info(api_link, p.Results.max_nb_series);

if isempty(series_list)
    df = {};
    return
else
    common_columns = {'x_frequency', 'provider_code', 'dataset_code', 'dataset_name', 'series_code', 'series_name','original_period', 'period', 'original_value', 'value'};
    % Compute dimensions_labels_columns_names and dimensions_codes_columns_names
    dimensions_labels_columns_names = {};
    dimensions_codes_columns_names = {};
    
    dataset_codes = fieldnames(datasets_dimensions);
    for ii = 1:length(dataset_codes)
        dataset_dimensions = datasets_dimensions.(dataset_codes{ii});
        dimensions_codes_columns_names = dataset_dimensions.dimensions_codes_order';
        for jj = 1:length(dataset_dimensions.dimensions_codes_order)
            if isfield(dataset_dimensions, 'dimensions_labels') && isfield(dataset_dimensions, 'dimensions_values_labels')
                try
                    dimensions_labels_columns_names_dataset{jj} = dataset_dimensions.dimensions_labels.(dataset_dimensions.dimensions_codes_order{jj});
                catch
                    dataset_dimensions.dimensions_codes_order{jj} = regexprep(dataset_dimensions.dimensions_codes_order{jj},'[^a-zA-Z0-9]','_');
                    dimensions_labels_columns_names_dataset{jj} = dataset_dimensions.dimensions_labels.(dataset_dimensions.dimensions_codes_order{jj});
                end    
            else
                if isfield(dataset_dimensions, 'dimensions_values_labels')
                    dimensions_labels_columns_names_dataset{jj} = [dataset_dimensions.dimensions_codes_order{jj} '_label'];
                end
            end
        end
        dimensions_labels_columns_names = [dimensions_labels_columns_names, dimensions_labels_columns_names_dataset];
    end
    ordered_columns_names = [common_columns, dimensions_codes_columns_names, dimensions_labels_columns_names];
    
    if ~isempty(p.Results.dbnomics_filters)
        filtered_series_list = filter_series(series_list, p.Results.dbnomics_filters,p.Results.editor_api_base_url);
        % Append common column names with period_middle_day and filtered columns for the final DataFrame
        idx_period = find(strcmp(ordered_columns_names, 'period'));
        idx_value = find(strcmp(ordered_columns_names, 'value'));
        ordered_columns_names = {ordered_columns_names{1:idx_period}, 'period_middle_day', ordered_columns_names{idx_period+1:idx_value}, 'filtered', ordered_columns_names{idx_value+1:end}};
        % Append series_list with the filtered series
        series_list = [series_list, filtered_series_list];
    end
    
    rows_ = 0;
    for s = 1:length(series_list)
        rows_ = rows_ + length(series_list{s}.value);
    end
    df = cell(rows_+1,length(ordered_columns_names));
    df(1,:) = ordered_columns_names;
    
    series_length=0;
    % Flatten series received from the API (rename some keys of JSON result to match cell array organization)
    for ii = 1:length(series_list)
        if ~isfield(series_list{ii}, 'filtered')
            flat_series = flatten_dbnomics_series(series_list{ii});

            % Add dimensions labels to flat_series
            complete_dataset_code = [flat_series.provider_code '_' flat_series.dataset_code];
            dataset_dimensions = datasets_dimensions.(complete_dataset_code);
            if isfield(dataset_dimensions, 'dimensions_labels')
                dataset_dimensions_labels = dataset_dimensions.dimensions_labels;
            else
                dataset_dimensions_labels = struct();
                for jj = 1:length(dataset_dimensions.dimensions_codes_order)
                    dataset_dimensions_labels.(dataset_dimensions.dimensions_codes_order{jj}) = [dataset_dimensions.dimensions_codes_order{jj} '_label'];
                end
            end
            % Add dimensions values labels to current series
            if isfield(dataset_dimensions, 'dimensions_values_labels')
                dimension_codes  = intersect(dimensions_codes_columns_names, fieldnames(dataset_dimensions_labels)');
                for jj = 1:length(dimension_codes)
                    try
                        series_code = regexprep(flat_series.series_code,'[^a-zA-Z0-9]','_');
                        dimension_label = dataset_dimensions_labels.(dimension_codes{jj});
                        flat_series.labels{jj, 1} = dimension_label;
                        dimension_value_code = regexprep(series_dims_by_dataset_code.(complete_dataset_code).(series_code).(dimension_codes{jj}),'[^a-zA-Z0-9]','_');
                    catch
                        % Avoid structure fieldnames to start with scalars 
                        series_code_temp = regexprep(series_code(1),'[0-9]','A');
                        series_code = [series_code_temp series_code];
                        dimension_label = dataset_dimensions_labels.(dimension_codes{jj});
                        flat_series.labels{jj, 1} = dimension_label;
                        dimension_value_code = regexprep(series_dims_by_dataset_code.(complete_dataset_code).(series_code).(dimension_codes{jj}),'[^a-zA-Z0-9]','_');
                    end
                    if isstrprop(dimension_value_code(1), 'digit') %MATLAB doesn't allow struct field names to start with a digit
                        dimension_value_code = strcat('x', dimension_value_code);
                    end
                    try
                        flat_series.labels{jj, 2} = dataset_dimensions.dimensions_values_labels.(dimension_codes{jj}).(dimension_value_code);
                    catch
                        for it = 1:size(dataset_dimensions.dimensions_values_labels.(dimension_codes{jj}), 1)
                            tmp = regexprep(dataset_dimensions.dimensions_values_labels.(dimension_codes{jj}){it}{1}, '[^a-zA-Z0-9]', '_');
                            if strcmp(tmp, dimension_value_code)
                                flat_series.labels{jj, 2} = dataset_dimensions.dimensions_values_labels.(dimension_codes{jj}){it}{2};
                            end
                        end
                    end
                end
            end
            if ~isempty(p.Results.dbnomics_filters)
                flat_series.filtered = false;
            end
        else
            flat_series = series_list{ii};
        end
        
        % Create final cell array
        for col = 1:length(ordered_columns_names)
            col_ = ordered_columns_names{col};
            for jj = 1:length(flat_series.value)
                if strcmp(col_, 'original_value') || strcmp(col_,'value') || strcmp(col_, 'original_period') || strcmp(col_, 'period')
                    df{series_length+jj+1,col} = flat_series.(col_){jj};
                elseif any(strcmp(col_,dimensions_labels_columns_names))
                    if isfield(flat_series, 'labels')
                        if ~any(strcmp(col_,flat_series.labels))
                            df{series_length+jj+1,col} = NaN;
                        else
                            idx = find(strcmp(flat_series.labels, col_));
                            df{series_length+jj+1,col} = flat_series.labels{idx,2};
                        end
                    else
                        df{series_length+jj+1,col} = NaN;
                    end
                elseif any(strcmp(col_, dimensions_codes_columns_names)) && ~any(strcmp(col_,fieldnames(flat_series)'))
                    df{series_length+jj+1,col} = NaN;
                elseif strcmp(col_, 'period_middle_day')
                    if isfield(flat_series, 'period_middle_day')
                        df{series_length+jj+1,col} = flat_series.(col_){jj};
                    else
                        df{series_length+jj+1,col} = NaN;
                    end
                else
                    df{series_length+jj+1,col} = flat_series.(col_);
                end
            end
        end
        series_length=series_length+length(flat_series.value);
    end
end
end

%@test:1    % test_fetch_series_by_api_link
%$ try
%$    df = fetch_series_by_api_link('https://api.db.nomics.world/v22/series/BIS/long_pp?limit=1000&offset=0&q=&observations=1&align_periods=1&dimensions=%7B%7D');
%$    t(1) = 1;
%$ catch
%$    t = 0;
%$ end
%$
%$ if t(1) 
%$    t(2) = dassert(length(unique(df(2:end,2))),1); 
%$    t(3) = dassert(df(2,2), {'BIS'}); 
%$    t(4) = dassert(length(unique(df(2:end,3))),1); 
%$    t(5) = dassert(df(2,3), {'long_pp'}); 
%$    t(6) = dassert(length(unique(df(2:end,5))),23); 
%$ end 
%$
%$ T = all(t);
%@eof:1