function [datasets_dimensions, series_dims_by_dataset_code, series_list] = iter_series_info(api_link, max_nb_series)
% function iter_series_info(api_link, max_nb_series)
% Makes API request. Iterates through series.docs returned by the API. 
% Returns structs of the dataset(s)' dimensions and series.
% The answer can have a key 'dataset_dimensions' if only one dataset is returned by API, or 'datasets_dimensions' if
% more than one dataset is returned.
%
% INPUTS
%   api_link                [string]           modified API link
%   max_nb_series           [integer]          maximum number of series requested by the API
%
% OUTPUTS
%   datasets_dimensions, series_dims_by_dataset_code, series_list
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

default_max_nb_series = 50;
total_nb_series = 0;

datasets_dimensions = struct();
series_dims_by_dataset_code = struct();

while (true)
    response_json = fetch_series_page(api_link, total_nb_series);
    series_page = response_json.series;
    num_found = series_page.num_found;
    
    if isnan(max_nb_series) && num_found > default_max_nb_series
        error('DBnomics Web API found %i series matching your request, %i, but you did not pass any value for the "max_nb_series" argument, so a default value of %i was used. Please give a higher value (at least max_nb_series=%i), and try again.', ...
               num_found, default_max_nb_series, num_found);
    end
    
    page_nb_series = length(series_page.docs);
    total_nb_series = total_nb_series+page_nb_series;
    
    if ~isnan(max_nb_series)
        if total_nb_series == max_nb_series
            break;
        elseif total_nb_series > max_nb_series
            nb_remaining_series = page_nb_series - (total_nb_series - max_nb_series);
            series_page.docs = series_page.docs(1:nb_remaining_series-1);
        end
    end
    
    series_list = cell(1,length(series_page.docs));
    
    for ii = 1:length(series_page.docs)
        try
            series = series_page.docs{ii};
        catch
            series = series_page.docs(ii);
        end
        series.provider_code = regexprep(series.provider_code,'[^a-zA-Z0-9]','_');
        series.dataset_code = regexprep(series.dataset_code,'[^a-zA-Z0-9]','_');
        complete_dataset_code = [series.provider_code '_' series.dataset_code];
        if numel(fieldnames(datasets_dimensions)) == 0
            assert(isfield(response_json, 'datasets') || isfield(response_json, 'dataset'));
            if isfield(response_json, 'datasets')
                datasets_dimensions = response_json.datasets;
            else
                datasets_dimensions.(complete_dataset_code) = response_json.dataset;
            end
        end
        
        series_list{ii} = series;
        % Store series dimensions information for future use
        series_code = regexprep(series.series_code,'[^a-zA-Z0-9]','_');
        try
            series_dims_by_dataset_code.(complete_dataset_code).(series_code) = series.dimensions;
        catch
            % Avoid structure fieldnames to start with scalars 
            series_code_temp = regexprep(series_code(1),'[0-9]','A');
            series_code = [series_code_temp series_code];
            series_dims_by_dataset_code.(complete_dataset_code).(series_code) = series.dimensions;
        end
        
    end

    assert(total_nb_series <= num_found);
    if total_nb_series == num_found
        break;
    end
end
end
